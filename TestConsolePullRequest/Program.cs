﻿using System;

namespace TestConsolePullRequest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            IExecutor executor = new Executor();
            executor.ExecuteJob();
        }
    }
}
